# Encrypt / Decrypt file with AES-256-GCM

## Encrypt
```
aes e <filename>
```

## Decrypt
```
aes d <filename>
```